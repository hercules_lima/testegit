import React from 'react'
import ReactDOM from 'react-dom'
import Member from './member'
import Family from './family'

ReactDOM.render(
    <Family lastName="Maranhão">
        <Member name="Hércules"  />
        <Member name="Jaqueline"  />
    </Family>
,document.getElementById('app'))

